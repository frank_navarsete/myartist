package com.glitchcube.boundary;

import com.glitchcube.entity.Artist;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ArtistManager {

    @PersistenceContext
    EntityManager em;

    public Artist findById(long id) {
        System.out.println("find Artist with id " + id);
        return em.find(Artist.class, id);
    }

    public void removeById(long id) {
        try {
            System.out.println("deleted artist with id " + id);
            Artist artist = em.getReference(Artist.class, id); //Getting reference(proxy) to avoid nullpointer-exception
            this.em.remove(artist);
        } catch (EntityNotFoundException e) {
            //We want to remove it. No problem if it was not found.
        }
    }

    public List<Artist> findAll() {
        System.out.println("finding all artists");
        return em.createNamedQuery(Artist.FIND_ALL, Artist.class).getResultList();
    }

    public Artist save(Artist artist) {
        System.out.println("Saving artist = " + artist);
        return em.merge(artist);
    }

    public int deleteEveryArtist() {
        return em.createQuery("DELETE FROM Artist ").executeUpdate();
    }
}
