package com.glitchcube.boundary;

import com.glitchcube.entity.Artist;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@Stateless
@Path("artist")
public class ArtistResource {

    @Inject
    ArtistManager manager;

    @GET
    @Path("{id}")
    public Artist find(@PathParam("id") long id) {
        return manager.findById(id);

    }

    @GET
    public List<Artist> all() {
        return manager.findAll();
    }

    @POST
    public Response save(Artist artist, @Context UriInfo info) {
        Artist savedArtist = manager.save(artist);
        long id = savedArtist.getId();

        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    }

    @PUT
    @Path("{id}")
    public Artist update(@PathParam("id") long id, Artist artist) {
        artist.setId(id);
        return manager.save(artist);
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        manager.removeById(id);
    }

    @DELETE
    public JsonObject deleteAllArtists() {
        int artistDeleted = manager.deleteEveryArtist();
        return Json.createObjectBuilder().add("artistDeleted", artistDeleted).build();
    }
}
