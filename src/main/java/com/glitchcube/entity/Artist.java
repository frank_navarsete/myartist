package com.glitchcube.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@NamedQuery(name = Artist.FIND_ALL, query = "select a from Artist a")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Artist {

    @Id
    @GeneratedValue
    long id;

    String name;
    String spotifyID;

    public static final String FIND_ALL = "Artist.findAll";

    public Artist() {
    }

    public Artist(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSpotifyID() {
        return spotifyID;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", spotifyID='" + spotifyID + '\'' +
                '}';
    }
}
